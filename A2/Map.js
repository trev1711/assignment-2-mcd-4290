let map;

if('geolocation' in navigator) {
navigator.geolocation.getCurrentPosition(success, error);
} else {
error()
}

function success (location){
let melbourne= [145.0420733, -37.8770097];
    console.log(location)
    mapboxgl.accessToken = 'pk.eyJ1IjoianVzdGluMDk3IiwiYSI6ImNsMnZ3MDh6eTBnMGIzY212Zm5pbjEybjAifQ.xQJeUwS89ZdOJzrBFxIWxQ';
    map = new mapboxgl.Map({
    container: 'map', // container ID
    style: 'mapbox://styles/mapbox/streets-v11', // style URL
    center: melbourne, // starting position [lng, lat]
    zoom: 9 // starting zoom
    });
    
}


function buttonPress(){ 
        let cName = document.getElementById("countryName").value;
           
        let data= {
        country:cName,
        callback: "results"
        }
        
    webServiceRequest(" https://eng1003.monash/OpenFlights/airports/",data)
    updateLSData(LOCKER_DATA_KEY, data)  
       }

     function results(results){
         for(let i = 0; i < results.length; i++){
    console.log(results[i])         
    console.log(results[i].name)
    console.log(results[i].latitude)
    console.log(results[i].longitude)
             
    const marker = new mapboxgl.Marker({
    color: getRandomColor(results.length),
    }).setLngLat([results[i].longitude,results[i].latitude])
    .addTo(map);
        

        new mapboxgl.Popup({offset: 30})
        .setLngLat([results[i].longitude, results[i].latitude])
        .setHTML("<h100>"+ results[i].name+"</h10>")
        .addTo(map);
             
             
         }
     
     }
        
function buttonPress2(){ 
        let saName = document.getElementById("sourceAirportId").value;
           
        let data= {
        sourceAirport:saName,
        callback: "result"
        }
        
    webServiceRequest(" https://eng1003.monash/OpenFlights/routes/",data)
    updateLSData(LOCKER_DATA_KEY, data)  
       }

function result(results){
         for(let i = 0; i < results.length; i++){
    //if (results[i].sourceAirport === sourceAirportId){
          console.log(results[i])
    console.log(results[i].sourceAirport)
    console.log(results[i].sourceAirportId) 
    console.log(results[i].destinationAirport)
    console.log(results[i].destinationAirportId)  
    document.getElementById("myTexts").defaultValue = results[i].sourceAirport;         
    document.getElementById("myText").defaultValue = results[i].destinationAirport;
            
         }
}


      function getRandomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.round(Math.random() * 15)];
        }
        return color;
    }  

function summaryDoc() {
  window.location.assign("main_page.html")
 
  
}

function summary(){
    
}

function error(){
    alert("Geolocation not available")
}
}